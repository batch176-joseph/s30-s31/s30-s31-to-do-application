//Mini Activity: 
/*
	Setup a basic Express JS server.

*/

//[SECTION] Dependencies and Modules
	const exp = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//[SECTION] Server Setup
	const app = exp ();
	const port = 4000;

	app.use(exp.json());
	app.use(exp.urlencoded({extended: true}));


//[SECTION] Database Connection
	mongoose.connect('mongodb+srv://seanjoseph07:admin123@cluster0.tschi.mongodb.net/toDo176?retryWrites=true&w=majority', {
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,"Connection Error"));
	db.once('open',()=>console.log("Connected to MongoDB"));
	
//Add the task route
	//localhost:4000/tasks/
	app.use("/tasks", taskRoute)

//[SECTION] Entry Point Response
	app.listen(port, () => console.log(`Server running at port ${port}`));