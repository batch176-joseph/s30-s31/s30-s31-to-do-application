//Contains all the endpoints for our application
//We separate the routes such that "app.js" only contains information on the server

const express = require("express")

//Create a Router instance that functions as a middleware and routing system
//Allows us access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router()

//The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskControllers")

//[Routes]
//The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed

//Route for getting all the tasks
	router.get("/", (req, res) => {
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
		})

	/*router.get("/tasks/:id", (req, res) => {
			taskController.getTheTasks().then(resultFromController => res.send(resultFromController))
	})*/

//Route for creating a task
	router.post("/", (req, res) => {

			//The "createTask" function needs the data from the request body, so we need to supply it to the function
			//If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
		})


//Route for Deleting a task
	//call out the routing component to register a brand new endpoint.
	//when integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC
	//2 Types of End points
		//Static Route
			//-> unchanging, fixed, contant, steady
		//Dynamic Route
			//-> interchangeable or not fixed
	router.delete('/:task', (req, res) => {
		//identify the task to be executed within this endpoint
		//call out the proper function for this route and identify the source/provide of the function.
		//Determine wether the value inside the path variable is transmitted to the server.
		console.log(req.params.task);
		//place the value of the path variable inside its own container
		let taskId = req.params.task;
		//res.send('Hello from delete'); //this is only to check if the setup is correct

		//retrieve the identity of the task by inserting the ObjectId of the resource.
		//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path variable.
		//Path variable -> this will allow us to insert data within the scope of the URL.
			//What is variable? -> container, storage of information
		//When is a path variable useful?
			//-> When inserting only a single piece of information.
			//-> this is also useful when passing down information to REST API method that does NOT include a BODY section like 'GET'. 

		//upon executing this method, a promise will be initialized so we need to handle the result of the promise in our route module.
		taskController.deleteTask(taskId).then(resultNgDelete=>res.send(resultNgDelete));
	})


//Route for Updating task status (Pending -> Complete)
	//Let's create a dynamic endpoint for this brand new route.
router.put('/:task', (req, res) => {
	//check if are able to acquire the values inside the path variables.
	console.log(req.params.task); //check if the value inside the parameters of the route is properly transmitted to the server.
	let idNiTask = req.params.task;

	//identify the business logic behind this task inside the controller module.
	//call the intended controller to execute the process, make sure to identify the provider/sopurce of the function.
	//after invoking the controller method handle the outcome
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
});


//Route for Updating task status (Completed -> Pending)
	//This will be a counter procedure for the previous task.
router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	console.log(id);//test the initial setup

	//declare the business logic aspect of this brand new task in our app.
	//invoke the task you want to execute in this route.
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});

});



//We use "module.exports" to export the router object to use in the "app.js"
module.exports = router;